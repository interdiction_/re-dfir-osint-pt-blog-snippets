import "pe"

rule future_dtg
{
	meta:
		author = "inteloperator"
		description = "looks for Micheal J Fox DTGs"
		date = "05.09.21"
		score = 100
	strings:
		$x0 = { 4D 5A 90 00 03 } // MZ Hex
	condition:
        $x0 and pe.timestamp > 1641015021
}