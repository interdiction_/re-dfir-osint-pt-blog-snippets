rule HYP_Credential_Stealer : exposure_id credential_id
{
    meta:
        title = "Hypno Stealer"
        description = "In memory detection"
        stage = "stage_prod"
        date = "30082021"
        modified = "30082021"
        author = "inteloperator"
        falsepositives = "NO"
    strings:
        $hex01 = { 4D 5A 90 00 03 00 00 00 04 00 00 00 FF FF } // MZ Hex
        $badabing01 = "discord" nocase ascii
        $badabing02 = "ScannedWallet" nocase ascii
        $badabing03 = "BlockedCountry" nocase ascii
        $badabing04 = "card_number_encrypted" wide
        $badabing05 = "DownloadAndEx" nocase ascii
        $badabing06 = "GeoPlugin" nocase ascii
        $badabing07 = "tcp://" wide
        $badabing08 = "scannerArg" nocase ascii
        $badabing09 = "https://" wide
        $badabing10 = "StringDecrypt" nocase ascii
        $badabing11 = "StringFileInfo" wide
    condition:
    all of ($badabing*) and $hex01
}