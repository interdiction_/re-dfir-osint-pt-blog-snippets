### SET FOLDER TO WATCH + FILES TO WATCH + SUBFOLDERS YES/NO
$watcher = New-Object System.IO.FileSystemWatcher
$watcher.Path = "X:\dirty_files"
$watcher.Filter = "*.*"
$watcher.IncludeSubdirectories = $true
$watcher.EnableRaisingEvents = $true
### DEFINE ACTIONS AFTER AN EVENT IS DETECTED
$action = { $path = $Event.SourceEventArgs.FullPath
            $changeType = $Event.SourceEventArgs.ChangeType
            $logline = "$(Get-Date), $changeType, $path"
            Add-content "X:\metadataremoval.log" -value $logline
            Start-Process -NoNewWindow -erroraction 'silentlycontinue' -FilePath "X:\exiftool.exe" -ArgumentList "-all= $path" -wait
            {Start-Sleep 1}
            Start-Process -NoNewWindow -erroraction 'silentlycontinue' -FilePath "X:\exiftool.exe" -ArgumentList "-r $path -delete_original!" -wait
            {Start-Sleep 1}
            Move-Item -force -Path "$path" -Destination "X:\clean_files"
          }    
### DECIDE WHICH EVENTS SHOULD BE WATCHED 
Register-ObjectEvent $watcher "Created" -Action $action
while ($true) {Start-Sleep 5}