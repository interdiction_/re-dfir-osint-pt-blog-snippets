#!/bin/bash
if [ -z $1 ]; then
    echo "Usage: rdpshot [ip]"
    exit 1 
fi
#
output="output"
timeout=10
timeoutStep=2
host=$1
blue="\e[34m[*]\e[0m"
red="\e[31m[*]\e[0m"
green="\e[32m[*]\e[0m"
temp="/tmp/${host}.png"
function screenshot {
    screenshot=$1
    window=$2
    echo -e "${blue} Saving shot ${screenshot}"
    import -window ${window} "${screenshot}"
    echo -e "${blue} Completed..."
}
function isAlive {
    pid=$1
    kill -0 $pid 2>/dev/null
    if [ $? -eq 1 ]; then
        echo -e "${red} Failed to connect to ${host}"
        exit 1
    fi
}
function isTimedOut {
    t=$1
    if [ $t -ge $timeout ]; then
        echo -e "${red} Timed out connecting to ${host}"
        kill $!
        exit 1
    fi
}
export DISPLAY=:0
#rdesktop in the background
echo -e "${blue} Connecting to ${host}"
echo "yes" |rdesktop -a 16 $host &
pid=$!
#Get window id
window=
timer=0
while true; do
    #Check to see if we timed out
    isTimedOut $(printf "%.0f" $timer)
    #Check to see if the process is still alive
    isAlive $pid
    window=$(xdotool search --name ${host})
    if [ ! "${window}" = "" ]; then
        echo -e "${blue} Got window id: ${window}"
        break
    fi
    timer=$(echo "$timer + 0.1" | bc)
    sleep 0.1
done
#focus the RDP window
echo -e "${blue} Focus to ${window}"
xdotool windowfocus "${window}"
#
timer=0
while true; do
    #process didn't die
    isAlive $pid
    isTimedOut $timer
    #screenshot the window; if(black) wait
    screenshot "${temp}" "${window}"
    colors=$(convert "${temp}" -colors 5 -unique-colors txt:- | grep -v ImageMagick)
    if [ $(echo "${colors}" | wc -l) -eq 1 ]; then
        echo -e "${blue} Loading..."
        sleep $timeoutStep
    else
	echo -e "${green} Con loaded for ${host}"
        break
    fi
    timer=$((timer + timeoutStep))
done
rm ${temp}
#wait some more, just in case
sleep 4
#screenshot
if [ ! -d "${output}" ]; then
    mkdir "${output}"
fi
afterScreenshot="${output}/${host}.png"
screenshot "${afterScreenshot}" "${window}"
#close rdesktop
kill $pid
display "${output}/${host}.png" &